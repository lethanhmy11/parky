﻿namespace ParkyWeb
{
    public static class SD
    {
        public static string APIBaseUrl = "https://localhost:7297/";
        public static string NationalParkAPIPath = APIBaseUrl+"api/v1/Nationalparks/";
        public static string TrailAPIPath = APIBaseUrl+"api/v1/Trails/";
        public static string AccountAPIPath = APIBaseUrl + "api/v1/Users/";
    }
}
