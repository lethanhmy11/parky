﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ParkyAPI.Migrations
{
    public partial class DeleteTableNationParkDto : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Trails_NationalParkDto_NationalParkId",
                table: "Trails");

            migrationBuilder.DropTable(
                name: "NationalParkDto");

            migrationBuilder.AddForeignKey(
                name: "FK_Trails_NationalParks_NationalParkId",
                table: "Trails",
                column: "NationalParkId",
                principalTable: "NationalParks",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Trails_NationalParks_NationalParkId",
                table: "Trails");

            migrationBuilder.CreateTable(
                name: "NationalParkDto",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Created = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Established = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    State = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NationalParkDto", x => x.Id);
                });

            migrationBuilder.AddForeignKey(
                name: "FK_Trails_NationalParkDto_NationalParkId",
                table: "Trails",
                column: "NationalParkId",
                principalTable: "NationalParkDto",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
