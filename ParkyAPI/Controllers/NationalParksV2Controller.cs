﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ParkyAPI.Models;
using ParkyAPI.Models.Dtos;
using ParkyAPI.Repository.IRepository;

namespace ParkyAPI.Controllers
{
    [ApiVersion("2.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    //[Route("api/[controller]")]
    [ApiController]
   // [ApiExplorerSettings(GroupName = "ParkyOpenAPISpecNationalPark")]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public class NationalParksV2Controller : ControllerBase
    {
        private readonly INationalParkRepository nationalParkRepository;
        private readonly IMapper mapper;

        public NationalParksV2Controller(INationalParkRepository nationalParkRepository, IMapper mapper)
        {
            this.nationalParkRepository = nationalParkRepository;
            this.mapper = mapper;
        }
        /// <summary>
        /// Get list of national park
        /// </summary>
        /// <returns></returns>
        //[MapToApiVersion("2.0")]
        [HttpGet]
        [ProducesResponseType(200, Type= typeof(List<NationalParkDto>))]
        public IActionResult GetNationalParks()
        {
            var obj= nationalParkRepository.GetNationalParks().FirstOrDefault();

            //var objDto = new List<NationalParkDto>();
            //foreach(var item in objList)
            //{
            //    objDto.Add(mapper.Map<NationalParkDto>(item));
            //}
            return Ok(mapper.Map<NationalParkDto>(obj));
        }

    }
}
