﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ParkyAPI.Models;
using ParkyAPI.Models.Dtos;
using ParkyAPI.Repository.IRepository;

namespace ParkyAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    //[Route("api/[controller]")]
    [ApiController]
    //[ApiExplorerSettings(GroupName = "ParkyOpenAPISpecNationalPark")]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public class NationalParksController : ControllerBase
    {
        private readonly INationalParkRepository nationalParkRepository;
        private readonly IMapper mapper;
        private readonly ITrailReponsitory test;

        public NationalParksController(INationalParkRepository nationalParkRepository, IMapper mapper, ITrailReponsitory test)
        {
            this.nationalParkRepository = nationalParkRepository;
            this.mapper = mapper;
            this.test = test;
            //var db1 = nationalParkRepository._db;
            //var db2 = test._db;
        }
        /// <summary>
        /// Get list of national park
        /// </summary>
        /// <returns></returns>
        //[MapToApiVersion("1.0")]
        [HttpGet]
        [ProducesResponseType(200, Type= typeof(List<NationalParkDto>))]
        public IActionResult GetNationalParks()
        {
            var objList = nationalParkRepository.GetNationalParks();

            var objDto = new List<NationalParkDto>();
            foreach(var item in objList)
            {
                objDto.Add(mapper.Map<NationalParkDto>(item));
            }
            return Ok(objDto);
        }

        /// <summary>
        /// Get individual national park
        /// </summary>
        /// <param name="nationalParkId">The Id of national park</param>
        /// <returns></returns>
        [HttpGet("{nationalParkId:int}",Name ="GetNationalPark")]
        [ProducesResponseType(200, Type = typeof(NationalParkDto))]
        [ProducesResponseType(404)]
        //[Authorize]
        [ProducesDefaultResponseType]
        public IActionResult GetNationalPark(int nationalParkId)
        {
            var obj = nationalParkRepository.GetNationalPark(nationalParkId);
            if(obj == null)
            {
                return NotFound();
            }
            var objDto=mapper.Map<NationalParkDto>(obj);          
            return Ok(objDto);
        }

        [HttpPost]
        [ProducesResponseType(201, Type = typeof(NationalParkDto))]
        //[ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult Create([FromBody] NationalParkDto nationalParkDto)
        {
            if(nationalParkDto == null)
            { 
                return BadRequest(ModelState); 
            }

            if (nationalParkRepository.NationalParkExists(nationalParkDto.Name))
            {
                ModelState.AddModelError("", "National Park Exists!");
                return StatusCode(404, ModelState);
            }

            var nationalParkObj = mapper.Map<NationalPark>(nationalParkDto);
            if (!nationalParkRepository.Create(nationalParkObj))
            {
                ModelState.AddModelError("",$"Something went wrong when saving the record {nationalParkObj.Name} ");
                return StatusCode(500, ModelState);
            }
            return CreatedAtRoute("GetNationalPark", new { nationalParkId = nationalParkObj.Id}, nationalParkObj);
        }

        [HttpPatch("{nationalParkId:int}", Name = "UpdateNationalPark")]
        [ProducesResponseType(204)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult Update(int nationalParkId, [FromBody] NationalParkDto nationalParkDto)
        {
            if (nationalParkDto == null||nationalParkId!=nationalParkDto.Id)
            {
                return BadRequest(ModelState);
            }

            var nationalParkObj = mapper.Map<NationalPark>(nationalParkDto);
            if (!nationalParkRepository.Update(nationalParkObj))
            {
                ModelState.AddModelError("", $"Something went wrong when saving the record {nationalParkObj.Name} ");
                return StatusCode(500, ModelState);
            }
            return NoContent();
        }

        [HttpDelete("{nationalParkId:int}", Name = "DeleteNationalPark")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult Delete(int nationalParkId)
        {
            if (!nationalParkRepository.NationalParkExistsById(nationalParkId))
            {
                return NotFound();
            }

            var nationalParkObj = nationalParkRepository.GetNationalPark(nationalParkId);
            if (!nationalParkRepository.Delete(nationalParkObj))
            {
                ModelState.AddModelError("", $"Something went wrong when saving the record {nationalParkObj.Name} ");
                return StatusCode(500, ModelState);
            }
            return NoContent();
        }


    }
}
