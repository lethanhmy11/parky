﻿using ParkyAPI.Models;

namespace ParkyAPI.Repository.IRepository
{
    public interface INationalParkRepository
    {
       

        ICollection<NationalPark> GetNationalParks();
        NationalPark GetNationalPark(int id);
        bool NationalParkExists(string name);
        bool NationalParkExistsById(int id);
        bool Create(NationalPark nationalPark);
        bool Update(NationalPark nationalPark);
        bool Delete(NationalPark nationalPark);
        bool Save();
    }
}
