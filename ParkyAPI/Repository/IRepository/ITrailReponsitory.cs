﻿using ParkyAPI.Models;

namespace ParkyAPI.Repository.IRepository
{
    public interface ITrailReponsitory
    {
        ICollection<Trail> GetTrails();
        ICollection<Trail> GetTrailsInNationalPark(int npId);
        Trail GetTrail(int id);
        bool TrailExists(string name);
        bool TrailExists(int id);
        bool Create(Trail trail);
        bool Update(Trail trail);
        bool Delete(Trail trail);
        bool Save();
    }
}
