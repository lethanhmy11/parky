﻿using ParkyAPI.Data;
using ParkyAPI.Models;
using ParkyAPI.Repository.IRepository;

namespace ParkyAPI.Repository
{
    public class NationalParkRepository : INationalParkRepository
    {
        public ApplicationDbContext _db;

        public NationalParkRepository(ApplicationDbContext db)
        {
            _db = db;
        }

        public bool Create(NationalPark nationalPark)
        {
            _db.NationalParks.Add(nationalPark);
            return Save();
        }

        public bool Delete(NationalPark nationalPark)
        {
            _db.NationalParks.Remove(nationalPark);
            return Save();
        }

        public NationalPark GetNationalPark(int id)
        {
            return _db.NationalParks.FirstOrDefault(p=>p.Id == id);
        }

        public ICollection<NationalPark> GetNationalParks()
        {
            return _db.NationalParks.OrderBy(p=>p.Name).ToList();
        }

        public bool NationalParkExists(string name)
        {
            var value=_db.NationalParks.Any(p => p.Name.ToLower().Trim() == name.ToLower().Trim());
            return value;
        }

        public bool NationalParkExistsById(int id)
        {
            var value = _db.NationalParks.Any(p => p.Id==id);
            return value;
        }

        public bool Save()
        {
            return _db.SaveChanges()>0?true:false;
        }

        public bool Update(NationalPark nationalPark)
        {
            _db.NationalParks.Update(nationalPark);
            return Save();
        }
    }
}
