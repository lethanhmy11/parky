﻿using Microsoft.EntityFrameworkCore;
using ParkyAPI.Data;
using ParkyAPI.Models;
using ParkyAPI.Repository.IRepository;

namespace ParkyAPI.Repository
{
    public class TrailReponsitory : ITrailReponsitory
    {
        public ApplicationDbContext _db;

        public TrailReponsitory(ApplicationDbContext db)
        {
            _db = db;
        }

        public bool Create(Trail trail)
        {
            _db.Trails.Add(trail);
            return Save();
        }

        public bool Delete(Trail trail)
        {
            _db.Trails.Remove(trail);
            return Save();
        }

        public Trail GetTrail(int id)
        {
            return _db.Trails.Include(t => t.NationalPark).FirstOrDefault(t=>t.Id == id);
        }

        public ICollection<Trail> GetTrails()
        {
            return _db.Trails.Include(t => t.NationalPark).ToList();
        }

        public ICollection<Trail> GetTrailsInNationalPark(int npId)
        {
            return _db.Trails.Include(t=>t.NationalPark).Where(t=>t.NationalParkId==npId).ToList();
        }

        public bool Save()
        {
            return _db.SaveChanges()>0?true:false;
        }

        public bool TrailExists(string name)
        {
            var value=_db.Trails.Any(t=>t.Name == name);
            return value;
        }

        public bool TrailExists(int id)
        {
            var value = _db.Trails.Any(t => t.Id==id);
            return value;
        }

        public bool Update(Trail trail)
        {
            _db.Trails.Update(trail);
            return Save();
        }
    }
}
